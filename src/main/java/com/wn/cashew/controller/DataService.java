package com.wn.cashew.controller;

import java.util.List;

import javax.inject.Inject;

import com.wn.cashew.data.gpx.GpxType;
import com.wn.cashew.data.reader.FileDataReader;

public class DataService {

	@Inject
	FileDataReader fileDataReader;

	public List<GpxType> getAllData() {
		return fileDataReader.getAllDatasets();
	}

}
