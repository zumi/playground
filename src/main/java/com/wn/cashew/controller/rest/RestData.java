package com.wn.cashew.controller.rest;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.wn.cashew.data.gpx.GpxType;
import com.wn.cashew.data.mongodb.controller.MongoService;
import com.wn.cashew.data.reader.DataReader;

import rx.Observable;
import rx.Observer;
import rx.observers.Observers;
import rx.schedulers.Schedulers;

/**
 * A simple REST service which is able to say hello to someone using
 * HelloService Please take a look at the web.xml where JAX-RS is enabled And
 * notice the @PathParam which expects the URL to contain /json/David or
 * /xml/Mary
 *
 * @author bsutter@redhat.com
 */
@Path("/rest/")
@Singleton
public class RestData {

	@Inject
	DataReader dataService;

	@Inject
	MongoService mongoService;

	private int i = 0;

	@POST
	@Path("/getdata/")
	@Produces("application/json")
	public GpxType getData() {

		// try {
		// JAXBContext jc = JAXBContext.newInstance(GpxType.class);
		//
		// Marshaller marshaller = jc.createMarshaller();
		// marshaller.setProperty(MarshallerProperties.MEDIA_TYPE,
		// "application/json");
		// marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
		// marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		// marshaller.marshal(dataService.getAllDatasets().get(0), System.out);
		// } catch (JAXBException e) {
		// e.printStackTrace();
		// }
		//
		// System.out.println("call " + i++);
		// return dataService.getAllDatasets().get(0);

//		mongoService.test();
//		GpxType gpxType = dataService.getAllDatasets().get(0);
//		mongoService.storeTrack(gpxType);
//		mongoService.getTrack();
		
		Observer<Integer> soutObserver = new Observer<Integer>() {

			@Override
			public void onNext(Integer t) {
				System.out.println("Rx "+t);

			}

			@Override
			public void onError(Throwable e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onCompleted() {
				System.out.println("completed");

			}
		};

		
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		LogSimReactive testSIm = new LogSimReactive();
		Observable<Integer> temp = testSIm.subscribeTo();
		ArrayList<Future<Void>> futureList = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			LogSimReactive logSim = new LogSimReactive();
			temp = Observable.merge(temp, logSim.subscribeTo());
			logSim.subscribeTo().count().subscribe(soutObserver);
			futureList.add(executorService.submit(logSim));
		}
		try {
			testSIm.call();
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		temp.subscribeOn(Schedulers.io()).count().subscribe(soutObserver);
		
		for (Future<Void> future : futureList) {
			try {
				future.get();
			} catch (InterruptedException | ExecutionException e1) {
				e1.printStackTrace();
			}
		}
		return null;
	}

}
