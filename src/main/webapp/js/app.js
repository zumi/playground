angular.module('myApp', []).controller('myCtrl', function($scope, $http) {

	$scope.button1 = function() {
		$http({
			method : 'POST',
			url : 'http://localhost:8080/cashew/cashew/rest/getdata/'
		}).then(function(data, status, headers, config) {
			$scope.answer = data
		});
	}

	$scope.login = function() {
		$http({
			method : 'POST',
			url : 'http://localhost:8080/cashew/cashew/rest/login/',
			data : {
				username : $scope.user.username,
				password : $scope.user.password
			}
		}).then(function(data, status, headers, config) {
			$scope.token = data.data.token
		});

	}
	
	$scope.test = function() {
		$http({
			method : 'POST',
			url : 'http://localhost:8080/cashew/cashew/rest/test/',
			headers: {
				   'token': $scope.token
				 }
		}).then(function(data, status, headers, config) {
			$scope.token = data.data.token
		});

	}

});
