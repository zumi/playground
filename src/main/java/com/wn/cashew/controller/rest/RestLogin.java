package com.wn.cashew.controller.rest;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.KeyLengthException;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.wn.cashew.data.user.User;

@Path("/rest/")
@Singleton
public class RestLogin {

	@POST
	@Path("/login/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String getData(User user) {

		System.out.println(user.getUsername());
		System.out.println(user.getPassword());

		// Generate random 256-bit (32-byte) shared secret
		SecureRandom random = new SecureRandom();
		byte[] sharedSecret = new byte[32];
		random.nextBytes(sharedSecret);

		// Create HMAC signer
		JWSSigner signer = null;
		try {
			signer = new MACSigner(sharedSecret);
		} catch (KeyLengthException e1) {
			e1.printStackTrace();
			return "{ \"token\" : \"\" }";
		}

		Date issuetime = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
		Date expirationTime = Date.from(LocalDateTime.now().plusHours(1).atZone(ZoneId.systemDefault()).toInstant());

		// Prepare JWT with claims set
		JWTClaimsSet claimsSet = new JWTClaimsSet.Builder().subject(user.getUsername()).issuer("me")
				.issueTime(issuetime).expirationTime(expirationTime).build();

		SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);

		// Apply the HMAC protection
		try {
			signedJWT.sign(signer);
		} catch (JOSEException e) {
			e.printStackTrace();
			return "{ \"token\" : \"\" }";
		}

		return "{ \"token\" : \"" + signedJWT.serialize() + "\"}";
	}

	@POST
	@Path("/test/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String test(@HeaderParam("token") String token) {
		System.out.println(token);

		return "{ \"token\" : \"\" }";
	}

}
