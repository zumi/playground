package com.wn.cashew.data.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.wn.cashew.data.gpx.GpxType;

public class FileDataReader implements DataReader {

	public List<GpxType> getAllDatasets() {
		System.out.println("started");
		JAXBContext jc;
		List<GpxType> gpxList = new ArrayList<>();
		try {
			jc = JAXBContext.newInstance(GpxType.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();

			final Path p = Paths.get("/", "home", "marco", "workspace", "cashew", "testData");
			final PathMatcher filter = p.getFileSystem().getPathMatcher("glob:**.gpx");
			try (final Stream<Path> stream = Files.list(p)) {
				stream.filter(filter::matches).forEach((path) -> {
					try {
						JAXBElement<GpxType> element = (JAXBElement<GpxType>) unmarshaller.unmarshal(path.toFile());
						gpxList.add(element.getValue());
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		System.out.println(gpxList.size());
		System.out.println("done");
		return gpxList;
	}

}
