package com.wn.cashew.data.reader;

import java.util.List;

import com.wn.cashew.data.gpx.GpxType;

public interface DataReader {

	public List<GpxType> getAllDatasets();
	
}
