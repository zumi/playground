package com.wn.cashew.controller.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataParam;

@Path("/file")
public class RestFileUploader {

	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public void uploadFile(@FormDataParam(value = "file") List<InputStream> is) {

		System.out.println("POOOST");
		System.out.println(is.size());
		for (InputStream s : is) {
			System.out.println("*****************new file");
			try (BufferedReader br = new BufferedReader(new InputStreamReader(s))) {
				String str = null;
				while ((str = br.readLine()) != null) {
					System.out.println(str);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
