package com.wn.cashew.data.mongodb.controller;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.wn.cashew.data.gpx.GpxType;
import com.wn.cashew.data.gpx.TrkType;
import com.wn.cashew.data.gpx.WptType;

@Singleton
public class MongoService {

	// TODO: inject it
	MongoClient mongoClient = new MongoClient();

	public void test() {
		MongoDatabase db = mongoClient.getDatabase("test");

		// Document document = new Document("username", "marco");
		// document.append("password", "1234");
		// document.append("subdocument", new Document().append("subkey",
		// "subvalue"));
		//
		// Document document1 = new Document("username", "bob");
		// document1.append("password", "1234");
		// document1.append("subdocument", new Document().append("subkey",
		// "subvalue"));
		//
		// db.getCollection("user").insertMany(Arrays.asList(document,
		// document1));

//		db.getCollection("user").updateOne(new Document("username", "bob"),
//				new Document("$set", new Document("password", "yxcv")));
//
//		FindIterable<Document> iterable = db.getCollection("user").find();
//
//		iterable.forEach(new Block<Document>() {
//			@Override
//			public void apply(final Document document) {
//				System.out.println(document);
//			}
//		});
		
		db.getCollection("tracks").drop();
	}

	public void storeTrack(GpxType gpxType) {
		MongoDatabase db = mongoClient.getDatabase("test");

		TrkType trackType = gpxType.getTrk().get(0);
		Document trackDocument = new Document("trackName", trackType.getName());
		// document.append("time", trackType.get)
		List<Document> wpDocList = new ArrayList<>();
		for (WptType wp : trackType.getTrkseg().get(0).getTrkpt()) {
			Document wpDocument = new Document();
			wpDocument.append("lat", wp.getLat().doubleValue());
			wpDocument.append("lon", wp.getLon().doubleValue());
			wpDocument.append("ele", wp.getEle().doubleValue());
			wpDocument.append("time", wp.getTime().toGregorianCalendar().getTime());
			wpDocList.add(wpDocument);
		}
		trackDocument.append("waypoints", wpDocList);
		
		db.getCollection("tracks").insertOne(trackDocument);
		

//		Document document = new Document("username", "marco");
//		document.append("password", "1234");
//		document.append("subdocument", new Document().append("subkey", "subvalue"));
//
//		Document document1 = new Document("username", "bob");
//		document1.append("password", "1234");
//		document1.append("subdocument", new Document().append("subkey", "subvalue"));
//
//		db.getCollection("user").insertMany(Arrays.asList(document, document1));

//		db.getCollection("user").updateOne(new Document("username", "bob"),
//				new Document("$set", new Document("password", "yxcv")));

//		FindIterable<Document> iterable = db.getCollection("user").find();

//		iterable.forEach(new Block<Document>() {
//			@Override
//			public void apply(final Document document) {
//				System.out.println(document);
//			}
//		});
	}
	
	public void getTrack(){
		MongoDatabase db = mongoClient.getDatabase("test");
		Document firstTrack = db.getCollection("tracks").find().first();
		System.out.println(firstTrack.toJson());
	}
}
