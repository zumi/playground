package com.wn.cashew.controller.rest;

import java.util.concurrent.Callable;

import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.ReplaySubject;

public class LogSimReactive implements Callable<Void> {
	
	ReplaySubject<Integer> subject = ReplaySubject.create();
	
	@Override
	public Void call() throws Exception {
		int counter = 0;
		for (int i = 0; i < 10000; i++) {
			// log
			try {
				Thread.sleep(0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			// notify
			subject.onNext(i);
			counter++;
		}
		
		System.out.println("actual count: "+counter);
		subject.onCompleted();
		return null;
	}
	
	public Observable<Integer> subscribeTo(){
		return subject;//.subscribeOn(Schedulers.newThread());
	}

}
